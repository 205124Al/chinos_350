<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1 class="text-info">Lista de paises</h1>
    <table class="table table-hover table-bordered">
        <thead>
           <tr>
             <th class="text-info">Pais</th>
             <th class="text-info">Capital</th>
             <th class="text-info">Moneda</th>
             <th class="text-info">Población</th>
             <th class="text-info">Ciudades destacadas</th>
           </tr>
        </thead>
        <tbody class="text-info">
        <!--recorro la tabla foreach blade -->
        @foreach($paises as $pais => $infopais)
        <tr>
        <td rowspan="3">{{ $pais }}</td>
        <td rowspan="3">{{$infopais ["capital"] }}</td>
        <td rowspan="3">{{$infopais ["moneda"]}}</td>
        <td rowspan="3">{{$infopais ["población"]}}</td>
        <td>{{ $infopais ["ciudades"][0]   }}</td>
        </tr>
        <tr>
        <th>{{$infopais ["ciudades"] [1]}}</th>
        </tr>
        <tr>
        <th>{{$infopais ["ciudades"] [2]}}</th>
        </tr>

        @endforeach
        </tbody>
    </table>
</body>
</html>