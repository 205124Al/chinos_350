<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Ruta de prueba
Route::get('hola', function(){
    echo "hola";
});

//Ruta de arreglo 
Route::get('arreglo' , function(){

    //defino un arreglo
    $estudiantes = [ "AN" => "Ana" ,
      "MA" => "Maria" ,
      "VA" => "Valeria" ,
      "CA" => "Carlos"];
  //ciclos foreach: recorrer arreglo

  foreach($estudiantes as $indice => $e){
      echo "$e  tiene el indice: $indice <br />" ;
  }
});

//Ruta de paises 
Route::get('paises' , function(){

 $paises=[
      "Colombia" => [
          "capital" => "Bogotá D.C",
          "moneda" => "Peso Colombiano",
          "población" => 50372424.0,
          "ciudades" => ["Medellin", "Cali", "Barranquilla"]
      ],
      "Peru" => [
        "capital" => "Lima",
        "moneda" => "Soles",
        "población" => 33050325.0,
        "ciudades" => ["Arequipa", "Cusco", "Trujillo"]
      ],
      "Ecuador" => [
        "capital" => "Quito",
        "moneda" => "Dolar",
        "población" => 17517141.0,
        "ciudades" => ["Guayaquil", "Ambato", "Cuenca"]
      ],
      "Brazil" => [
        "capital" => "Brasilia",
        "moneda" => "Real",
        "población" => 212216052.0,
        "ciudades" => ["Rio de Janeiro", "São Paulo", "Manaos"]
      ]
 ];

 //Enviar datos de paises a una vista
 //con la funcion view de laravel
 return view ("paises")->with("paises", $paises);

});
